// Find all our documentation at https://docs.near.org
import {NearBindgen, near, call, view, Vector} from 'near-sdk-js';
import {Investor} from "./models/Investor";
import {Reward} from "./models/Reward";

@NearBindgen({})
class Milestone {

    lastId: number = 1
    name: string = "Milestone - Ui Living"
    tldr: string = 'Unidad cercana a puntos clave en GDL, múltiples amenidades en la torre y departamento con 57m2.'
    image: string = 'https://res.cloudinary.com/dgcxcqu6p/image/upload/v1685022876/Imagen_de_la_Unidad-min_djal5a.png'
    brochure: string = 'https://drive.google.com/file/d/1dKm2f7CFAabGsEPRFuE_GiqnY495x47Y/view?usp=share_link'
    details: string = "Milestone - Ui Living. El primer desarrollo descentralizado con 2 Torres de 16 niveles. Ubicado a 1 minuto de la Universidad Panamericana de Guadalajara,a 1 minuto del parque metropolitano, a 3 minutos de plaza Galerías. Amenidades dentro de la torre: -Coliving Space-Coworking Space -Market -Salón de eventos -Poolside -Canchas de pádel -Área ZEN -Firepit Las unidad es de 57.2 Mt2 con 1 recámara."
    total: number = 3552000
    block_cost: number = 32000

    balance: string = ''

    demoInvestmentFactor: number = 1000000
    listOfInvestors: Vector<Investor> = new Vector<Investor>('list-of-investors');
    listOfRewards: Vector<Reward> = new Vector<Reward>('list-of-rewards');

    @view({})
    get_project_details(): object {
        return {
            name: this.name,
            tldr: this.tldr,
            image: this.image,
            brochure: this.brochure,
            details: this.details,
            total: this.total,
            block_cost: this.block_cost
        }
    }

    @view({})
    get_all_investors(): Investor[] {
        return this.listOfInvestors.toArray()
    }

    @view({})
    get_my_buys(): Investor[] {
        return this.listOfInvestors.toArray().filter(investor => investor.wallet === near.signerAccountId())
    }

    @view({})
    get_all_balance(): string {
        return this.balance
    }

    @call({payableFunction: true})
    set_new_investor({quantity, wallet}: {
        quantity: number,
        wallet: string
    }): void {
        const finalAmount: bigint = near.attachedDeposit() as bigint

        const newInvestor = {
            id: this.lastId,
            date: new Date().toString(),
            quantity,
            wallet: near.signerAccountId(),
            total: finalAmount
        }
        let partial = BigInt(this.balance)
        partial += finalAmount
        this.balance = partial.toString()

        // Mint NFT

        this.listOfInvestors.push(newInvestor)
        this.lastId += 1
    }
}