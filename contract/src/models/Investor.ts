export class Investor {
    id: number
    date: string
    quantity: number
    wallet: string
    total: bigint
}