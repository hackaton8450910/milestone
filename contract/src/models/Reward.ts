export class Reward {
    investor_wallet: string
    payment_type: string
    status: string
    amount: number
    hash: string
}