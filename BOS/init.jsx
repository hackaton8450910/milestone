const contractName = "dev-1684986732129-35378090621082";

const result = Near.view(contractName, "get_all_investors", "{}");
const contractData = Near.view(contractName, "get_project_details", "{}");

console.log("All investors", result);
console.log("Contract data", contractData);

State.init({
	message: "Hello world my state",
});

const testCall = () => {
	Near.call(contractName, "set_new_investor", {
		quantity: 2,
		wallet: "vicl9403.testnet",
	});
};

return (
	<>
		{/* src="near/widget/Onboarding.ComponentCard" to be pasted below */}
		<Widget />
		<h2>Hola</h2>
		<pre>{result}</pre>
		<div class="container border border-info p-3 text-center">
			<p> {state.message} </p>
		</div>

		<div class="mb-3">
			<label for="exampleFormControlInput1" class="form-label">
				Email address
			</label>
			<input
				type="email"
				class="form-control"
				id="exampleFormControlInput1"
				placeholder="name@example.com"
			/>
		</div>
		<div class="mb-3">
			<label for="exampleFormControlTextarea1" class="form-label">
				Example textarea
			</label>
			<textarea
				class="form-control"
				id="exampleFormControlTextarea1"
				rows="3"
			></textarea>
			<button onClick={testCall} class="btn btn-primary">
				Invertir
			</button>
		</div>
		{/* src="near/widget/Onboarding.ComponentCollection" to be pasted below */}
		<Widget />
	</>
);
